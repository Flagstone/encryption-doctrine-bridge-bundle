<?php
/**
 *  EncryptionDoctrineExtension.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2022/04/28
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Twig;

use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces\EncryptionDatabaseInterface;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;

class EncryptionDoctrineExtension implements ExtensionInterface
{
    private DatabaseEncryption $encryption;

    /**
     *  EncryptionDoctrineExtension constructor.
     *
     *  @param DatabaseEncryption $encryption
     */
    public function __construct(DatabaseEncryption $encryption)
    {
        $this->encryption = $encryption;
    }

    /**
     *  Attach bundle's filters to all twig filters
     *
     *  @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('decrypt', [$this, 'decrypt'])
        ];
    }

    /** *************************************************************************************************************
     *  @param string $value
     *  @param EncryptionDatabaseInterface $entity
     *  @param string $property
     *  @return string
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function decrypt(string $value, EncryptionDatabaseInterface $entity, string $property): string
    {
        return $this->encryption->decryptData(get_class($entity), $property, $value);
    }

    public function getTokenParsers(): array
    {
        return [];
    }

    public function getNodeVisitors(): array
    {
        return [];
    }

    public function getTests(): array
    {
        return [];
    }

    public function getFunctions(): array
    {
        return [];
    }

    public function getOperators(): array
    {
        return [];
    }
}