<?php
/**
 * EncryptionDatabaseInterface.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces;

/**
 * Interface EncryptionDatabaseInterface
 * | Interface for entities that want to use Encryption capabilities, and manage data persistence (Listener).
 * | The two methods have to be implements to avoid (or not) the EncryptionListener.
 * @package Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces
 */
interface EncryptionDatabaseInterface
{
    /**
     * @return  bool
     */
    public function isEncryptionListenerEnabled(): bool;

    /**
     * @param   bool $encryptionListenerEnabled
     * @return  mixed
     */
    public function setEncryptionListenerEnabled(bool $encryptionListenerEnabled);
}