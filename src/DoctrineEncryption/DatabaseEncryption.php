<?php
/**
 *  DatabaseEncryption.php
 *  @copyright  2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  Created: 2022/04/26
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption;

use Doctrine\Common\Annotations\AnnotationReader;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Annotations\EncryptionAnnotation;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces\EncryptionDatabaseInterface;
use ReflectionClass;
use ReflectionException;

class DatabaseEncryption
{
    private AnnotationReader $annotationReader;

    private EncryptionClassFactory $encryptionFactory;

    const ANNOTATION_CLASS  = 'Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Annotations\EncryptionAnnotation';
    const ENTITY_INTERFACE  = 'Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces\EncryptionDatabaseInterface';
    const CRYPT             = 0;
    const DECRYPT           = 1;

    /**
     * DatabaseEncryption constructor.
     * @param AnnotationReader          $reader
     * @param EncryptionClassFactory    $encryptionFactory
     */
    public function __construct(AnnotationReader $reader, EncryptionClassFactory $encryptionFactory)
    {
        $this->annotationReader = $reader;
        $this->encryptionFactory = $encryptionFactory;
    }

    /**
     *  Crypt an attribute of en entity
     *
     *  @param  string  $class
     *  @param  string  $attribute
     *  @param  string  $value
     *  @return string
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    public function cryptData(string $class, string $attribute, string $value): string
    {
        return $this->crypt($class, $attribute, $value, self::CRYPT);
    }

    /**
     *  Decrypt an attribute of en entity
     *
     *  @param  string  $class
     *  @param  string  $attribute
     *  @param  string  $value
     *  @return string
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    public function decryptData(string $class, string $attribute, string $value): string
    {
        return $this->crypt($class, $attribute, $value, self::DECRYPT);
    }

    /**
     * @param   string  $class
     * @param   string  $attribute
     * @param   string  $value
     * @param   int     $method
     * @return  null|string
     * @throws  Exceptions\EncryptionClassUndefinedException
     * @throws  ReflectionException
     */
    public function crypt(string $class, string $attribute, string $value, int $method): ?string
    {
        $reflectionClass = new ReflectionClass($class);
        if (in_array(self::ENTITY_INTERFACE, $reflectionClass->getInterfaceNames())) {
            $reflectionProperty = $reflectionClass->getProperty($attribute);
            /** @var EncryptionAnnotation $annotation */
            $annotation = $this->annotationReader->getPropertyAnnotation($reflectionProperty, self::ANNOTATION_CLASS);
            if (null !== $annotation) {
                $encodedClass = $this->encryptionFactory->build($annotation->getEncoderClass());
                if (self::CRYPT === $method) {
                    return $encodedClass->encode($value);
                }
                return $encodedClass->decode($value);
            }
        }
        return null;
    }

    /**
     * Crypt an attribute of an entity
     *
     * @param   string  $class
     * @param   string  $attribute
     * @param   string  $value
     * @return  string|null
     * @throws  Exceptions\EncryptionClassUndefinedException
     * @throws  ReflectionException
     */
    public function cryptField(string $class, string $attribute, string $value): ?string
    {
        return $this->crypt($class, $attribute, $value, self::CRYPT);
    }

    /**
     * Decrypt an attribute of an entity
     *
     * @param   string  $class
     * @param   string  $attribute
     * @param   string  $value
     * @return  string|null
     * @throws  Exceptions\EncryptionClassUndefinedException
     * @throws  ReflectionException
     */
    public function decryptField(string $class, string $attribute, string $value): ?string
    {
        return $this->crypt($class, $attribute, $value, self::DECRYPT);
    }

    /**
     * Crypt or decrypt all fields with @Encryption annotation in an entity. Rewrite all properties
     *
     * @param   EncryptionDatabaseInterface $entity
     * @param   int                         $method
     * @return  EncryptionDatabaseInterface
     * @throws  ReflectionException|Exceptions\EncryptionClassUndefinedException
     */
    public function cryptFields(EncryptionDatabaseInterface $entity, int $method): EncryptionDatabaseInterface
    {
        $entityClass = get_class($entity);
        $reflectedClass = new ReflectionClass($entityClass);
        $properties = $reflectedClass->getProperties();

        foreach ($properties as $property) {
            /** @var EncryptionAnnotation $annotation */
            $annotation = $this->annotationReader->getPropertyAnnotation($property, self::ANNOTATION_CLASS);
            if (null !== $annotation) {
                $getter = 'get' . ucfirst($property->getName());
                $setter = 'set' . ucfirst($property->getName());

                $encodedString = $this->crypt($entityClass, $property->getName(), $entity->$getter(), $method);
                if (null !== $encodedString) {
                    $entity->$setter($encodedString);
                }
            }
        }
        return $entity;
    }

    /**
     * Crypt all fields of an entity
     *
     * @param   EncryptionDatabaseInterface $entity
     * @return  EncryptionDatabaseInterface
     * @throws  Exceptions\EncryptionClassUndefinedException
     * @throws  ReflectionException
     */
    public function cryptAllFields(EncryptionDatabaseInterface $entity): EncryptionDatabaseInterface
    {
        return $this->cryptFields($entity, self::CRYPT);
    }

    /**
     * Decrypt all fields of an entity
     *
     * @param   EncryptionDatabaseInterface $entity
     * @return  EncryptionDatabaseInterface
     * @throws  Exceptions\EncryptionClassUndefinedException
     * @throws  ReflectionException
     */
    public function decryptAllFields(EncryptionDatabaseInterface $entity): EncryptionDatabaseInterface
    {
        return $this->cryptFields($entity, self::DECRYPT);
    }

    /**
     *  Return all encrypted field name for an entity
     *
     *  @param  EncryptionDatabaseInterface $entity
     *  @return array
     *  @throws ReflectionException
     */
    public function getEncryptedFieldsName(EncryptionDatabaseInterface $entity): array
    {
        $fieldsName = [];
        $entityClass = get_class($entity);
        $reflectedClass = new ReflectionClass($entityClass);
        $properties = $reflectedClass->getProperties();
        foreach ($properties as $property) {
            $annotation = $this->annotationReader->getPropertyAnnotation($property, self::ANNOTATION_CLASS);
            if (null !== $annotation) {
                $fieldsName[] = $property->getName();
            }
        }
        return $fieldsName;
    }

    /**
     *  Test if an $entityBefore (encrypted) is equal to an $entityAfter(decrypted) after encrypted
     *
     *  @param string $valueBefore
     *  @param string $valueAfter
     *  @param string $class
     *  @param string $field
     *  @return bool
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    public function hasDataChanged(string $valueBefore, string $valueAfter, string $class, string $field): bool
    {
        return $valueBefore !== $this->crypt($class, $field, $valueAfter, self::CRYPT);
    }

}