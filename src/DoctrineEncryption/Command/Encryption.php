<?php
/**
 *  Encryption.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2022/04/26
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Command;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\EncryptionClassFactory;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces\EncryptionDatabaseInterface;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption as Crypt;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 *  Class Encryption
 *
 *  Some method to facilitate Command encryption and decryption
 *  @package Flagstone\EncryptionDoctrineBridgeBundle\Command
 */
class Encryption
{
    private EntityManagerInterface $entityManager;

    private Reader $annotationReader;

    private EncryptionClassFactory $encryptionFactory;

    private Crypt $encryption;

    /** *************************************************************************************************************
     *  Encryption constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param EntityManagerInterface $entityManager
     *  @param Reader $annotationReader
     *  @param EncryptionClassFactory $encryptionFactory
     *  @param Crypt $encryption
     *  ************************************************************************************************************* */
    public function __construct(EntityManagerInterface $entityManager, Reader $annotationReader, EncryptionClassFactory $encryptionFactory, Crypt $encryption)
    {
        $this->entityManager = $entityManager;
        $this->annotationReader = $annotationReader;
        $this->encryptionFactory = $encryptionFactory;
        $this->encryption = $encryption;
    }

    /** *************************************************************************************************************
     *  Encrypt or decrypt a field for en entity
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $entity
     *  @param string $field
     *  @param int $mode
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function cryptField(string $entity, string $field, int $mode): void
    {
        $begin = microtime(true);
        $output = new ConsoleOutput();
        $reflectionClass = new ReflectionClass($entity);

        $reflectionProperty = $reflectionClass->getProperty($field);
        $annotation = $this->annotationReader->getPropertyAnnotation($reflectionProperty, Crypt::ANNOTATION_CLASS);

        if (null === $annotation) {
            CommandUtils::displayError('Property \''.$field.'\' doesn\'t have @Encryption annotation and cannot be encrypted.');
            return;
        }

        /**
         * @var EncryptionDatabaseInterface[]
         */
        $data = $this->entityManager->getRepository($entity)->findAll();

        $getter = 'get' . ucfirst($field);
        $setter = 'set' . ucfirst($field);
        $nbRow = 0;
        CommandUtils::display('', 'green');
        $output->write('<fg=white>');
        foreach ($data as $dataToCipher) {
            $fieldValue = $dataToCipher->$getter();

            $encodedString = $this->encryption->crypt($entity, $field, $fieldValue, $mode);
            if (null !== $encodedString) {
                $dataToCipher->$setter($encodedString);
            }
            $dataToCipher->setEncryptionListenerEnabled(false);

            $output->write('.');
            $nbRow++;
        }
        $this->entityManager->flush();

        $output->writeln('</>');
        $timeToCipher = microtime(true) - $begin;
        $this->displayDone($nbRow, $timeToCipher, $mode);
    }

    /** *************************************************************************************************************
     *  Encrypt or decrypt an entity
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $entity
     *  @param int $mode
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function cryptEntity(string $entity, int $mode): void
    {
        $begin = microtime(true);
        $output = new ConsoleOutput();
        $reflectionClass = new ReflectionClass($entity);

        $properties = $reflectionClass->getProperties();

        /**
         * @var EncryptionDatabaseInterface[]
         */
        $data = $this->entityManager->getRepository($entity)->findAll();

        CommandUtils::display('', 'green');
        $output->write('<fg=white>');
        $nbRow = 0;
        foreach ($data as $dataToCipher) {
            foreach($properties as $property) {
                $annotation = $this->annotationReader->getPropertyAnnotation($property, Crypt::ANNOTATION_CLASS);
                if (null !==$annotation) {
                    $getter = 'get' . ucfirst($property->getName());
                    $setter = 'set' . ucfirst($property->getName());

                    $encodedString = $this->encryption->crypt($entity, $property->getName(), $dataToCipher->$getter(), $mode);
                    if (null !== $encodedString) {
                        $dataToCipher->$setter($encodedString);
                    }
                }
            }
            $dataToCipher->setEncryptionListenerEnabled(false);
            $output->write('.');
            $nbRow++;
        }
        $this->entityManager->flush();

        $output->writeln('</>');
        $timeToCipher = microtime(true) - $begin;
        $this->displayDone($nbRow, $timeToCipher, $mode);
    }

    /** *************************************************************************************************************
     *  Display the done result of encryption/decryption treatment
     *  -------------------------------------------------------------------------------------------------------------
     *  @param int $nbRow
     *  @param float $timeToCipher
     *  @param int $mode
     *  ************************************************************************************************************* */
    private function displayDone(int $nbRow, float $timeToCipher, int $mode): void
    {
        if (Crypt::CRYPT === $mode) {
            CommandUtils::display(sprintf($nbRow . ' row(s) encrypted, in %01.3f sec.', $timeToCipher), 'green');
        } else {
            CommandUtils::display(sprintf($nbRow . ' row(s) decrypted, in %01.3f sec.', $timeToCipher), 'green');
        }
    }
}
