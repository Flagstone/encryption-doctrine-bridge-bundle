<?php
/**
 *  CommandUtils.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2022/04/22
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Command;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Question\Question;

/** *****************************************************************************************************************
 *  Class CommandUtils
 *  -----------------------------------------------------------------------------------------------------------------
 *  Some method to facilitate Command Input and Output
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncryptionDoctrineBridgeBundle\Command
 *  ***************************************************************************************************************** */
class CommandUtils
{
    /** *************************************************************************************************************
     *  Display an error on the console
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $text
     *  ************************************************************************************************************* */
    public static function displayError(string $text): void
    {
        $output = new ConsoleOutput();
        $output->writeln([
            '<error>',
            str_repeat(' ', strlen($text) + 10),
            ' ERROR : ' . $text . ' ',
            str_repeat(' ', strlen($text) + 10),
            '</>'
        ]);
    }

    /**
     *  Display a string on the console
     *
     *  @param string       $text   Text to display
     *  @param string|null  $fg     Foreground color of the text
     *  @param string|null  $bg     Background color of the text
     */
    public static function display(string $text, ?string $fg = null, ?string $bg = null): void
    {
        $colorList = [null, 'blue', 'red', 'white', 'yellow', 'cyan', 'black', 'green'];
        if (!in_array($fg, $colorList) || !in_array($bg, $colorList)) {
            self::displayError('Color output is not valid');
            return;
        }
        $output = new ConsoleOutput();
        if (null !== $fg && null !== $bg) {
            $output->writeln([
                '',
                '<fg='.$fg.',bg='.$bg.'>' . $text . '</>',
                ''
            ]);
        } elseif (null !== $fg) {
            $output->writeln([
                '',
                '<fg='.$fg.'>' . $text . '</>',
                ''
            ]);
        } elseif (null !== $bg) {
            $output->writeln([
                '',
                '<bg='.$bg.'>' . $text . '</>',
                ''
            ]);
        }  else {
            $output->writeln([
                '',
                $text,
                ''
            ]);
        }
    }

    /**
     *  Display a question to the console
     *
     *  @param  string      $questionStr
     *  @param  bool        $required
     *  @param  string      $error
     *  @param  int|null    $filter
     *  @return string
     */
    public static function askQuestion(string $questionStr, bool $required, string $error, ?int $filter = null): string
    {
        $application = new Application();
        $output = new ConsoleOutput();
        $input = new ArgvInput();

        $helper = $application->getHelperSet()->get('question');
        $questionStr = (true === $required) ? $questionStr .= ' (required): ' : $questionStr .= ' : ';

        $question = new Question('<fg=magenta>'.$questionStr.'</>');
        $valueOk = false;

        if (null !== $filter) {
            while (false === $valueOk) {
                $value = $helper->ask($input, $output, $question);
                if (false !== filter_var($value, $filter)) {
                    return $value;
                } else {
                    CommandUtils::displayError($error);
                }
            }
        } else {
            $value = '';
            if (true == $required) {
                while (0 >= strlen($value)) {
                    $value = $helper->ask($input, $output, $question);
                    if (empty($value)) {
                        $output->writeln([
                            '<fg=red>'.$error.'</>',
                            ''
                        ]);
                    } else {
                        return $value;
                    }
                }
            } else {
                return $helper->ask($input, $output, $question);
            }
        }
        return '';
    }
}
