<?php
/**
 *  DecryptEntity.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2022/04/26
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Command;

use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption as Crypt;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;

/**
 *  Class DecipherEntity
 *
 *  Command to encrypt all fields that have @Encryption annotation on all entities
 *  @package Farvest\EncryptionDoctrineBridgeBundle\Command
 */
#[AsCommand(
    name: 'fs-encryption:entity:decrypt',
    description: 'Decrypt an entire entity'
)]
class DecryptEntity extends Command
{
    /**
     *  @var Encryption
     */
    private Encryption $encryption;

    /**
     *  DecryptEntity constructor.
     *  @param Encryption $encryption
     */
    public function __construct(Encryption $encryption)
    {
        parent::__construct();
        $this->encryption = $encryption;
    }

    /**
     *  Configuration of the command
     */
    protected function configure()
    {
        $this
            ->setDescription('Decrypt all data defined by its entity.')
            ->setHelp('/!\ Be sure to decrypt encrypted data. You can loose your data if this command is not correctly used.');
    }

    /**
     *  Execution of the command.
     *
     *  @param InputInterface $input
     *  @param OutputInterface $output
     *  @return void
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->write("\033\143");
        $output->writeln([
            '',
            '<fg=green>Flagstone Encryption Doctrine Bridge - Decrypt Entity</>',
            '<fg=yellow>====================================================</>',
            '',
            '<fg=green>Decrypt all database data defined by its entity.</>',
            '<fg=green>Before decrypt an entity, you must add @Encryption annotation on all fields of the entity you want to decrypt.</>',
            '',
            '<fg=red>/!\ Be sure to decrypt encrypted data. You can loose your data if this command is not correctly used.</>',
            '<fg=red>You can decrypt data by using the decrypt command (make sure that base and string are not changed).</>',
            '<fg=yellow>=====================================================================================================</>',
        ]);

        $entity = CommandUtils::askQuestion(
            'Entity full name concern by the decryption',
            true,
            'Entity is required'
        );

        $this->encryption->cryptEntity($entity, Crypt::DECRYPT);
    }
}
