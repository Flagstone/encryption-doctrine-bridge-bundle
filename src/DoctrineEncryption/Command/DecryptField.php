<?php
/**
 *  DecryptField.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2022/04/27
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Command;

use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption as Crypt;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ReflectionException;

/**
 *  Class DecryptField
 *
 *  Command to decrypt a field on all entities
 *  @package Flagstone\EncryptionDoctrineBridgeBundle\Command
 */
#[AsCommand(
    name: 'fs-encryption:field:decrypt',
    description: 'Decrypt a field in an entity'
)]
class DecryptField extends Command
{
    /**
     *  @var Encryption
     */
    private Encryption $encryption;

    /**
     *  CipherField constructor.
     *
     * @param Encryption $encryption
     */
    public function __construct(Encryption $encryption)
    {
        parent::__construct();
        $this->encryption = $encryption;
    }

    /**
     *  Configuration of the command
     */
    protected function configure()
    {
        $this
            ->setDescription('Decrypt all data defined by its entity and a field.')
            ->setHelp('/!\ Be sure to Decrypt encrypted data. You can loose your data if this command is not correctly used.');
    }

    /**
     *  Execution of the command.
     *
     *  @param  InputInterface  $input
     *  @param  OutputInterface $output
     *  @return void
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->write("\033\143");
        $output->writeln([
            '',
            '<fg=green>Flagstone Encryption Doctrine Bridge - DecryptField</>',
            '<fg=yellow>==================================================</>',
            '',
            '<fg=green>Decrypt all database data defined by its entity and its field.</>',
            '<fg=green>After decrypt a field, you must remove @Encryption annotation on the entity you want to decrypt.</>',
            '',
            '<fg=red>/!\ Be sure to decrypt encrypted data. You can loose your data if this command is not correctly used.</>',
            '<fg=red>You can crypt data by using the crypt command (make sure that base and string are not changed).</>',
            '<fg=yellow>=====================================================================================================</>',
        ]);

        $entity = CommandUtils::askQuestion(
            'Entity full name concern by the decryption',
            true,
            'Entity is required'
        );

        $field = CommandUtils::askQuestion(
            'Property of the entity concern by the decryption',
            true,
            'Property is required'
        );

        $this->encryption->cryptField($entity, $field, Crypt::DECRYPT);
    }
}