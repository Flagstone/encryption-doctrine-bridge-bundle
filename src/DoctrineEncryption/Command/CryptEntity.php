<?php
/**
 *  CryptEntity.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2019/01/09
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Command;

use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption as Crypt;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'fs-encryption:entity:crypt',
    description: 'Encrypt an entity'
)]
class CryptEntity extends Command
{
    private Encryption $encryption;

    public function __construct(Encryption $encryption)
    {
        parent::__construct();
        $this->encryption = $encryption;
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Crypt all data defined by its entity.')
            ->setHelp('/!\ Be sure to crypt decrypted data. You can loose your data if this command is not correctly used.');
    }

    /**
     * @param   InputInterface  $input
     * @param   OutputInterface $output
     * @return  void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->write("\033\143");
        $output->writeln([
            '',
            '<fg=green>Flagstone Encryption Doctrine Bridge - Crypt Entity</>',
            '<fg=yellow>==================================================</>',
            '',
            '<fg=green>Crypt all database data defined by its entity.</>',
            '<fg=green>Before encrypt an entity, you must add @Encryption annotation on all fields of the entity you want to crypt.</>',
            '',
            '<fg=red>/!\ Be sure to crypt decrypted data. You can loose your data if this command is not correctly used.</>',
            '<fg=red>You can decrypt data by using the decrypt command (make sure that base and string are not changed).</>',
            '<fg=yellow>=====================================================================================================</>',
        ]);

        $entity = CommandUtils::askQuestion(
            'Entity full name concern by the encryption',
            true,
            'Entity is required'
        );

        try {
            $this->encryption->cryptEntity($entity, Crypt::CRYPT);
        } catch (EncryptionClassUndefinedException $e) {
            $output->writeln([
                '',
                '<fg=red>EncryptionClassUndefinedException - Crypt Entity</>',
                '<fg=red>================================================</>',
                '',
                '<fg=red>'.$e->getMessage().'</>',
                '<fg=red>================================================</>',
            ]);
        } catch (ReflectionException $e) {
            $output->writeln([
                '',
                '<fg=red>ReflectionExceptionError - Crypt Entity</>',
                '<fg=red>=======================================</>',
                '',
                '<fg=red>'.$e->getMessage().'</>',
                '<fg=red>=======================================</>',
            ]);
        }
    }
}