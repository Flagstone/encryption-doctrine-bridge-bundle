<?php
/**
 *  CryptField.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2022/04/27
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Command;

use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption as Crypt;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ReflectionException;

/**
 *  Class CryptField
 *
 *  Command to encrypt a field on all entities
 *  @package Flagstone\EncryptionDoctrineBridgeBundle\Command
 */
#[AsCommand(
    name: 'fs-encryption:field:crypt',
    description: 'Encrypt a field of an entity'
)]
class CryptField extends Command
{
    /**
     *  @var Encryption
     */
    private Encryption $encryption;

    /**
     *  CipherField constructor.
     *
     * @param Encryption $encryption
     */
    public function __construct(Encryption $encryption)
    {
        parent::__construct();
        $this->encryption = $encryption;
    }

    /**
     *  Configuration of the command
     */
    protected function configure()
    {
        $this
            ->setDescription('Crypt all data defined by its entity and a field.')
            ->setHelp('/!\ Be sure to crypt decrypted data. You can loose your data if this command is not correctly used.');
    }

    /**
     *  Execution of the command.
     *
     *  @param  InputInterface  $input
     *  @param  OutputInterface $output
     *  @return void
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->write("\033\143");
        $output->writeln([
            '',
            '<fg=green>Flagstone Encryption Doctrine Bridge - Crypt Field</>',
            '<fg=yellow>=================================================</>',
            '',
            '<fg=green>Crypt all database data defined by its entity and its field.</>',
            '<fg=green>Before crypt a field, you must add @Encryption annotation on the entity you want to encrypt.</>',
            '',
            '<fg=red>/!\ Be sure to crypt decrypted data. You can loose your data if this command is not correctly used.</>',
            '<fg=red>You can decrypt data by using the decrypt command (make sure that base and string are not changed).</>',
            '<fg=yellow>=====================================================================================================</>',
        ]);

        $entity = CommandUtils::askQuestion(
            'Entity full name concern by the encryption',
            true,
            'Entity is required'
        );

        $field = CommandUtils::askQuestion(
            'Property of the entity concern by the encryption',
            true,
            'Property is required'
        );

        $this->encryption->cryptField($entity, $field, Crypt::CRYPT);
    }
}