<?php
/**
 * EncryptionAnnotation.php
 *
 * @copyright 2022
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Annotations;

use Attribute;

/**
 * Represents an attribute for marking a property to be encrypted.
 * This annotation allows specifying an encoder class and an encoder string to customize the encryption mechanism.
 */
#[Attribute(Attribute::TARGET_PROPERTY)]
class EncryptionAnnotation
{
    public ?string $encoderClass = null;
    public string $encoderString;

    public function __construct(?string $encoderClass = null, string $encoderString = '')
    {
        $this->encoderClass = $encoderClass;
        $this->encoderString = $encoderString;
    }

    public function getEncoderClass(): ?string
    {
        return $this->encoderClass;
    }

    public function getEncoderString(): string
    {
        return $this->encoderString;
    }
}