<?php
/**
 * EncryptionClassFactory.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption;

use Flagstone\EncryptionBundle\Encryption\Interfaces\EncryptionInterface;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;

/**
 * Class EncryptionClassFactory
 * | Create the encryption class to use based on the class name
 * @package Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption
 */
class EncryptionClassFactory
{
    /**
     * @var string
     */
    private string $defaultEncoder;

    /**
     * EncryptionClassFactory constructor.
     * @param   string $defaultEncoder
     * @throws  EncryptionClassUndefinedException
     */
    public function __construct(string $defaultEncoder)
    {
        $this->defaultEncoder = $defaultEncoder;

        if (!class_exists($defaultEncoder)) {
            throw new EncryptionClassUndefinedException(sprintf('The encoder class \'%s\' doesn\'t exists', $defaultEncoder));
        }
    }

    /**
     * | Return the encryption class defined by the class name. If class name is null, return the default class
     * | defined in the bundle parameters.
     * @param string|null $encryptionClass
     * @return EncryptionInterface
     * @throws EncryptionClassUndefinedException
     */
    public function build(?string $encryptionClass): EncryptionInterface
    {
        if (null === $encryptionClass) {
            return new $this->defaultEncoder();
        }

        if (class_exists($encryptionClass)) {
            return new $encryptionClass();
        }

        throw new EncryptionClassUndefinedException(sprintf('The encoder class \'%s\' doesn\'t exists', $encryptionClass));
    }
}