<?php
/**
 * EncryptionClassUndefinedException.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions;

use Exception;

/**
 * Class EncryptionClassUndefinedException
 * @package Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions
 */
class EncryptionClassUndefinedException extends Exception
{

}