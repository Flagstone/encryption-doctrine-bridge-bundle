<?php
/**
 *  EncryptionListener.php
 *  @copyright  2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  Created: 2022/04/26
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\EventListener;

use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces\EncryptionDatabaseInterface;
use ReflectionException;

/**
 *  Class EncryptionListener
 *  Listener for encrypt data on an entity.
 *  @package Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\EventListener
 */

class EncryptionListener
{
    private DatabaseEncryption $encryption;

    public function __construct(DatabaseEncryption $encryption)
    {
        $this->encryption = $encryption;
    }

    /**
     *  Operation to do before entities with EncryptionDatabaseInterface Interface is updated
     *  Operation is made only if isEncryptionListenerEnabled is set to TRUE, to avoid multiple calls
     *
     *  @param PreUpdateEventArgs $event
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    public function preUpdate(PrePersistEventArgs $event): void
    {
        if (!$event->getObject() instanceof EncryptionDatabaseInterface) {
            return;
        }
        /**
         * @var EncryptionDatabaseInterface
         */
        $entity = $event->getObject();
        if (true === $entity->isEncryptionListenerEnabled()) {
            $this->cryptFields($entity);
        }
    }

    /**
     *  Operation to do before entities with EncryptionDatabaseInterface Interface is created
     *  Operation is made only if isEncryptionListenerEnabled is set to TRUE, to avoid multiple calls
     *
     *  @param PrePersistEventArgs $event
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    public function prePersist(PrePersistEventArgs $event): void
    {
        if (!$event->getObject() instanceof EncryptionDatabaseInterface) {
            return;
        }
        /**
         * @var EncryptionDatabaseInterface
         */
        $entity = $event->getObject();
        if (true === $entity->isEncryptionListenerEnabled()) {
            $this->cryptFields($entity);
        }
    }

    /**
     *  Make the encryption.
     *
     *  @param $entity
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     */
    private function cryptFields($entity): void
    {
        $this->encryption->cryptAllFields($entity);
    }

}