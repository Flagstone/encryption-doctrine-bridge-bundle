<?php
/**
 * FlagstoneEncryptionDoctrineBridgeBundle.php
 *
 * @copyright 2021
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionDoctrineBridgeBundle;

use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

/**
 * Class FlagstoneEncryptionDoctrineBridgeBundle
 * @package Flagstone\EncryptionDoctrineBridgeBundle
 */
class FlagstoneEncryptionDoctrineBridgeBundle extends AbstractBundle
{

}